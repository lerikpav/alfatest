import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.LongStream;

public class Main {

    public static void main(String[] args) throws IOException {
        task2();
        task3();
    }

    private static void task2() throws IOException {
        System.out.println("Task 2: get values from file and sort them");
        String[] numbers = Files.readString(new File(Main.class.getResource("text.txt").getFile()).toPath()).split(",");
        List<String> strings = Arrays.asList(numbers);
        //Print increasing order
        strings.sort(Comparator.comparingInt(Integer::valueOf));
        System.out.println(strings);
        //Print decreasing order
        Collections.reverse(strings);
        System.out.println(strings);

    }

    private static void task3() {
        System.out.println("Task 3: print factor of 20");
        System.out.println(LongStream.range(1, 21).reduce((left, right) -> left * right).getAsLong());
    }
}
